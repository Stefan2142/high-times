import time
import glob
import pandas as pd
import numpy as np
from Tkinter import *
import Tkinter
import tkFileDialog
import psycopg2
import sqlalchemy
from sqlalchemy.orm import sessionmaker, scoped_session
def connect(user, password, db, host='3.80.191.123', port=5432):
    '''Returns a connection and a metadata object'''
    url  = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)

    # The return value of create_engine() is our connection object
    conn = sqlalchemy.create_engine(url, client_encoding='utf8')

    # We then bind the connection to MetaData()
    meta = sqlalchemy.MetaData(bind=conn, reflect=True)

    Session = scoped_session(sessionmaker(bind=conn)) #Engine

    s = Session()

    return conn, meta, s

def main():
    keywords = ['Investors.csv','Prospective_', 'Subscribers.csv']
    tables = ['Investors','ProspectiveInvestors', 'Subscribers']
    files = []
    conn, meta, s = connect('m_ht', '3zxfenra', 'HighTimes')

    # Needed for writing into db with pandas
    meta = sqlalchemy.MetaData(conn, schema='public')
    meta.reflect()
    pdsql = pd.io.sql.SQLDatabase(conn, meta=meta)

    for file in glob.glob("*.csv"):
        files.append(file)

    for table in tables:
        s.execute('delete from public."{}"'.format(table))
        # s.execute("delete from sqlite_sequence where name='{}'".format(table))
        s.commit()

    for table in tables:
        for fl in files:
            # print(keywords[tables.index(table)].lower(), fl.lower())
            if keywords[tables.index(table)].lower() in fl.lower():
                print table
                df = pd.read_csv(fl)

                pdsql.to_sql(df, table, if_exists='append') #replace, append, fail, 'index' - optional

                s.commit()
                break
    s.close()

if __name__ == '__main__':
    main()
"""
psql -d HighTimes -U postgres
\? list all the commands
\l list databases
\conninfo display information about current connection
\c [DBNAME] connect to new database, e.g., \c template1
\dt list tables of the public schema
\dt <schema-name>.* list tables of certain schema, e.g., \dt public.*
\dt *.* list tables of all schemas
Then you can run SQL statements, e.g., SELECT * FROM my_table;(Note: a statement must be terminated with semicolon ;)
\q quit psql
"""


# https://gist.github.com/00krishna/9026574