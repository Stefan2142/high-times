import time
import glob
import pandas as pd
import numpy as np
from Tkinter import *
import Tkinter
import tkFileDialog
import sqlite3

def db():
	try:
		conn = sqlite3.connect('HighTimes.db')
		return conn
	except Error as e:
		# print(e)
		logger.error('Problem with connection to db: %s'%(e))
		return None
def main():
	keywords = ['Investors.csv','Prospective_', 'Subscribers.csv']
	tables = ['Investors','ProspectiveInvestors', 'Subscribers']
	files = []
	conn = db()
	c = conn.cursor()

	for file in glob.glob("*.csv"):
		files.append(file)

	for table in tables:
		c.execute("delete from {}".format(table))
		c.execute("delete from sqlite_sequence where name='{}'".format(table))
		conn.commit()

	for table in tables:
		for fl in files:
			# print(keywords[tables.index(table)].lower(), fl.lower())
			if keywords[tables.index(table)].lower() in fl.lower():
				print table
				df = pd.read_csv(fl)

				df.to_sql(table, conn, if_exists='append', index=False)

				tmp = conn.execute('SELECT * from {}'.format(table)).fetchall()
				conn.commit()
				# for i in tmp:
				# 	print i
				break
	conn.close()

if __name__ == '__main__':
	main()
