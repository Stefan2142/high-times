import psycopg2
try:
    connection = psycopg2.connect(user = "postgres",
                                  password = "3zxfenra",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "HighTimes")
    cursor = connection.cursor()
    
    create_table_query = '''

CREATE TABLE Subscribers (
    ID  SERIAL PRIMARY KEY,
    value   INT,
    gen TEXT,
    fn  TEXT,
    ln  TEXT,
    dob TEXT,
    age INT,
    doby    INT,
    ct  TEXT,
    st  TEXT,
    zip INT,
    country TEXT,
    phone   TEXT,
    email   TEXT
);

'''
    
    cursor.execute(create_table_query)
    connection.commit()
    print("Table created successfully in PostgreSQL ")
except (Exception, psycopg2.DatabaseError) as error :
    print ("Error while creating PostgreSQL table", error)
finally:
    #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")