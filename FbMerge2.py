import config
import time
import glob
import datetime
import pandas as pd
import numpy as np
from pandas import pivot_table
from datetime import timedelta
import pygsheets
from Tkinter import *
import Tkinter
import Tkconstants
import tkFileDialog
# import UserList
# import UserString
# import UserDict
# import itertools
# import collections
# import future.backports.misc
# import commands
# import base64
# import math
# import reprlib
# import functools
# import re
# import subprocess
date = datetime.datetime.now().strftime("%Y-%m-%d %H-%M")


def upload_to_gsheet(df):
    """Deals with uploading the dataframe to google sheets using api.

    For GoogleSheetsAPI client, credentials need to be regulated first.
    Instructions can be found on: 
        https://pygsheets.readthedocs.io/en/stable/authorization.html
    Sheet name is HightTimes-Report-<date_time>.


    Arguments:
        df {pandas dataframe} -- dataframe thats going to be uploaded to google sheets
    """
    print("Uploading to gsheet...")
    gc = pygsheets.authorize(client_secret='./client_secret.json')
    sh = gc.create('HighTimes-Report-{}'.format(date))

    # select the first sheet
    wks = sh[0]

    wks.set_dataframe(df, (1, 1), copy_index=True)

    # Conditional formatting (color scale)
    # sh.custom_request(config.cond_format(str(wks.id)),'')
    print("PivotTable uploaded to gsheet, url:\n{}".format(wks.url))

    # Set sheet to public
    sh.share('', role='reader', type='anyone')
    print("PivotTable set to public.")


def from_tmdlt(t):
    """Converts decimal time into string

    Used for going back to the original format of the column 
    "Avg. Session Duration". 


    Arguments:
        t {float} -- time hh:mm:ss represented as decimal value

    Returns:
        string -- time represented as string in format hh:mm:ss
    """
    try:
        diff = timedelta(hours=t)
        return str(datetime.timedelta(seconds=diff.seconds))
    except Exception as e:
        print e, 'from tmdlt'


def tmdlt(t):
    """Converts time hh:mm:ss to decimal

    String t (hh:mm:ss) is splitted by ":" so operations can be
    performed to individual segments.

    Arguments:
        t {string} -- hh:mm:ss string

    Returns:
        float -- time in decimal format
    """
    try:
        HMS = [60 * 60, 60, 1]
        dec_time = sum(a * b for a, b in zip(HMS, map(int, t.split(":"))))
        dec_time /= 3600.
        return dec_time
    except Exception as e:
        print e, 'tmdlt', t


def check_reg_users_col(fl):
    """Determines which of google files have 'Registed Users' column.

    Only one of two google analytics file has 'Registered Users' column.
    We have to find it in order to merge only that column with the other file

    Arguments:
        fl {string} -- path to file

    Returns:
        bool -- True for founded column, False for not found.
    """
    data = pd.read_csv(fl, skiprows=6)
    for column in data.columns:
        if 'Registered' in column:
            return True
    return False


def main():
    fb_frames_l = []
    reg_col_idx = None

    root = Tk()
    root.withdraw()
    print("Select Facebook files..")
    fb_files = root.filename = tkFileDialog.askopenfilenames(
        initialdir="./", title="Select file",
        filetypes=(("csv files", "*.csv"), ("all files", "*.*")))

    print("Select Google files...")
    google_files = root.filename = tkFileDialog.askopenfilenames(
        initialdir="./", title="Select file",
        filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
    google_files = list(google_files)

    if len(google_files) != 2:
        print("Two Google files are not selected. Exiting...")
        return 0

    for fl in google_files:
        if check_reg_users_col(fl):
            reg_col_idx = google_files.index(fl)
            break
    print "Reg col idx:{}".format(reg_col_idx)

    for fb_file in fb_files:
        data = pd.read_csv(fb_file)
        data['Ad Name'] = data['Ad Name'].str.lower()
        for column in data.columns:
            if column not in config.fb_header_to_keep:
                try:
                    del data[column]
                except KeyError:
                    print "Couldnt del column {}".format(column)
                    continue

        data = data.drop(0)
        fb_frames_l.append(data)

    fb_merged = pd.concat(fb_frames_l, ignore_index=True)

    tmp = pd.read_csv(google_files[reg_col_idx], skiprows=6)
    del google_files[reg_col_idx]
    google_pd = pd.read_csv(google_files[0], skiprows=6)
    google_pd = pd.concat(
        [google_pd, tmp['Registered Users (Goal 4 Completions)']], axis=1)
    df_new = pd.DataFrame()

    df = pd.merge(google_pd, fb_merged,
                  left_on='Ad Content', right_on='Ad Name')


    print("Raw, merged data saved to raw.csv")
    df.to_csv(r'raw.csv', index=True, header=True)

    df = df.drop(columns="Source / Medium") # Remove

    # Format the values from string to its intended type (int, float, date, etc..)
    df.groupby("Ad Set Name")
    df['Users'] = df['Users'].astype(int)
    df['New Users'] = df['New Users'].astype(int)
    df['Sessions'] = df['Sessions'].astype(int)
    df['Bounce Rate'] = (pd.to_numeric(df['Bounce Rate'].str.strip("%")))
    df['Bounce Rate'] = df['Bounce Rate'].astype(float)
    df['Pages / Session'] = df['Pages / Session'].astype(float)
    df['Registered Users (Goal 4 Completions)'].astype(float)

    df['Avg. Session Duration'] = (pd.to_datetime(
        df['Avg. Session Duration'], format="%H:%M:%S"))
    df['Avg. Session Duration'] = df[
        'Avg. Session Duration'].dt.strftime("%H:%M:%S")
    df['Avg. Session Duration'] = df['Avg. Session Duration'].apply(
        lambda x: tmdlt(x)).astype(float)
    

    df['Transactions'] = df['Transactions'].astype(int)
    df['Revenue'] = pd.to_numeric(df['Revenue'].replace(
        '[\$,)]', '', regex=True).astype(float))
    df['Impressions'] = df['Impressions'].astype(int)
    df['Amount Spent (USD)'] = df['Amount Spent (USD)'].astype(float)

    df['Relevance Score'] = df['Relevance Score'].apply(
        pd.to_numeric, errors='coerce')
    
    df['Frequency'] = df['Frequency'].astype(float)

    # with default, dropna on True, margins(total row) would total wrong
    pv = pivot_table(df, index='Ad Set Name',
                     values=['Users', 'New Users', 'Sessions',
                             'Bounce Rate', 'Pages / Session',
                             'Avg. Session Duration', 'Transactions',
                             'Registered Users (Goal 4 Completions)',
                             'Revenue', 'Impressions', 'Amount Spent (USD)',
                             'Relevance Score', 'Frequency'],
                     aggfunc={"Users": np.sum,
                              "New Users": np.sum,
                              "Sessions": np.sum,
                              "Bounce Rate": np.mean,
                              "Pages / Session": np.mean,
                              'Avg. Session Duration': np.mean,
                              'Transactions': np.sum,
                              'Registered Users (Goal 4 Completions)': np.sum,
                              'Revenue': np.sum,
                              'Impressions': np.sum,
                              'Amount Spent (USD)': np.sum,
                              'Relevance Score': np.mean,
                              'Frequency': np.mean
                              }, margins=True, dropna=False)

    # Convert duration back from decimal to hh:mm:ss
    pv['Avg. Session Duration'] = pv['Avg. Session Duration'].apply(
        lambda x: from_tmdlt(x))

    # Fix column order by using config.py
    pv = pv.reindex_axis(config.pandas_column_order, axis=1)

    print("PivotTable:")
    with pd.option_context('expand_frame_repr', False):
        print(pv)

    # Don't forget to add '.csv' at the end of the path
    pv.to_csv(r'HightTimes-Report-{}.csv'.format(date), index=True, header=True)
    print("File saved locally as: HightTimes-Report-{}.csv".format(date))

    upload_to_gsheet(pv)


if __name__ == '__main__':
    main()
